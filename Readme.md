# HTTP Client schema check

This project uses [ajv](https://ajv.js.org/) to create a javascript file which can be included in
IntelliJ HTTP Client files to validate the schema of the responses.

## Usage
### general

You can import the exported functions from the generated file and use them to validate the response.
The function name is based on the operationId and the response code of the OpenAPI file.

This example imports the method for the request to `GET /pet/[petId]` with the response code `200`
and validates the response body.
See the full example in the file [examples/tests/petstore.http](examples/tests/petstore.http).

```javascript
import { getPetById_200 as validate } from '../schema/validate-petstore.js'
client.test("Response body is valid", function() {
    client.assert(validate(response.body), "Response body is invalid: " + JSON.stringify(validate.errors))
})
````

### run the generator
#### environment variables

The script searches for OpenAPI `*.yaml`-files in the directory passed in `INPUT_DIRECTORY` and creates the validation
script in the directory passed in `OUTPUT_DIRECTORY`.

`INPUT_DIRECTORY` and `OUTPUT_DIRECTORY` needs to be mounted to the container.
Their values are defaulting to the value of the variable `DIRECTORY` wich itself defaults to `/app/openapi`.

#### local
```shell
npm ci # install dependencies
npm run build # transpile the typescript code
export DIRECTORY=$(pwd)/examples/schema # set the directory
node create-validators.js # run the generator
```

> local execution creates some files in the current directory which are deleted after completion.

#### Container
```shell
cd examples
podman run --rm -it --security-opt="label=disable" -v $(pwd)/schema:/app/openapi registry.gitlab.com/http-client-schema-check/http-client-schema-check:latest
```

## How it works
1. Search for `*.yaml` files in `$INPUT_DIRECTORY`
2. Save information about the file in a buffer-array
3. search for operations in `paths` which have a `operationId` and a `responses` object
4. if the response may be of type `application/json` with a defined schema, save the path to the schema in a buffer-array together with the operationId and the response code
5. for each schema in the buffer-array, create an [ajv](https://ajv.js.org/) code generator object
   * `strict` needs to be false, because an OAS is not a valid json-schema file
   * `code` are the options for the generated code
     * `source` needs to be set to `true`, but I can't remember why and the docs are not helping to remember.
     * `es5` is necessary because the IntelliJ HTTP Client probably uses [Nashorn](https://openjdk.org/projects/nashorn) ([code](https://github.com/openjdk/nashorn)) which is an ES5 engine
     * `esm` is necessary because IntelliJ HTTP Client can only load ES6 modules
     * `optimize` is set to `false` because optimized code is not working with the engine
     * `lines` is set to `true` for better debugging
   * `unicodeRegExp` is set to `false` because the nashorn-engine cannot handle unicode RegExps
6. add ajvs default string formats
7. add the OAS as a meta-schema to the ajv-generator and don't validate it because it's not a valid json-schema.
   * The meta-schema is necessary to be able to reference the json-schemas defined in `components/schemas` in the OAS.
8. for each operation in the operations-buffer-array, create a simple json-schema which only references the real schema in the OAS. 
   * As key, `operationId` concatenated with `_` and the response code is used. This key is later used as the name for the exported validation functions.
9. run the ajv standalone code generator. This generates javascript-code independent of the ajv-core-library to validate any json-object against the schemas added to the generator.
   * This code still references to ajv-formats as well as some other checks like string-length as an external dependency by `require`ing these files.
10. Replace the `require`-statements to the format-definitions and other checks defined in external files with an `import` because webpack seems not to like mixing `require` when exporting as ES6 module.
11. IntelliJ HTTP Client can't find the ajv-formats when they are `import`ed and especially not when `require`d, so they need to be added to the generated code. This is done using [webpack](https://webpack.js.org/).
12. There are still language-features used in the generated code which are not supported by the Nashorn-engine. These are transplied using [babel](https://babeljs.io/).
    * unicode-regexes are replaced with non-unicode-regexes
    * usage of spread-syntax is expanded for arrays as well as objects
13. webpack writes a single `export` line for the functions to the bottom of the bundle-file. Nashorn doesn't like that, so this line gets moved to the top.
14. write the generated code to a file in the `$OUTPUT_DIRECTORY`.

## TODO
* refactor code into a `src` directory with object-oriented structure
* allow json files as input
* stdin and stdout for single file processing
* Architecture documentation
* make it installable via npm / create a "binary"
* create a man file and reference it in the README as well as `package.json`
