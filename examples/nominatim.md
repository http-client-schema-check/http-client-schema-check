```shell
curl -L https://github.com/sparkfabrik/nominatim-openapi/raw/main/docs/nominatim.openapi.json | yq -p=json | sed 's~$id~#$id~g' > examples/nominatim.yaml
```

The replacement of `$id` is necessary to prevent "Error: reference "https://semver.org/semver.schema.json" resolves to more than one schema"
