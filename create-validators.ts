import Ajv2020 from "ajv/dist/2020.js";
import addFormats from "ajv-formats";
import standaloneCode from "ajv/dist/standalone/index.js";
import * as yaml from "js-yaml";
import * as fs from "fs";
import {glob} from "glob";
import * as path from "path";
import webpack from 'webpack';
interface OasOperationConfig {
    id: string
    code: string
    path: string
}
interface OasConfig {
    name: string
    schema: any
    operationConfigs: OasOperationConfig[]
}

const directory = process.env.DIRECTORY || "./openapi"
const inputDirectory = process.env.INPUT_DIRECTORY || directory
const outputDirectory = process.env.OUTPUT_DIRECTORY || directory

const oasFileNames = await glob(inputDirectory + '/*.yaml')
const oasConfigs: OasConfig[] = []

// collect all responses of operation which have a operationId and a application/json with a response schema
for (let oasFileName of oasFileNames) {
    const oasFileBasename = path.basename(oasFileName, '.yaml')
    const schema = yaml.load(fs.readFileSync(oasFileName, 'utf8'))
    const operationConfigs: OasOperationConfig[] = []
    for (let path in schema["paths"]) {
        for (let operation in schema["paths"][path]) {
            const operationId = schema["paths"][path][operation]["operationId"]
            if (operationId !== undefined && schema["paths"][path][operation]["responses"]) {
                for (let code in schema["paths"][path][operation]["responses"]) {
                    const content = schema["paths"][path][operation]["responses"][code]["content"]
                    if (content && content["application/json"] && content["application/json"]["schema"]) {
                        const schemaPath = '#/paths/' + path.replace(/\//g, '~1') + "/" + operation + "/responses/" + code + "/content/application~1json/schema"
                        operationConfigs.push({id: operationId, path: schemaPath, code: code})
                    }
                }
            }
        }
    }
    if (operationConfigs.length >= 1) {
        oasConfigs.push({name: oasFileBasename, schema: schema, operationConfigs: operationConfigs})
    }
}

// replace the require()-methods with es module pendants. This is necessary because webpack doesn't want to mix.
// I could also find no way to transpile commonjs modules to ES modules
function replace_require(code: string) {
    const re_func = new RegExp("var (?<func_name>func[0-9]+) = require\\(\"(?<target>[a-z0-9/]+)\"\\)\\.(?<method_name>[a-z]+);", "gim")
    code = code.replace(re_func, "import $<func_name>_plain from \"$<target>.js\";\nvar $<func_name> = $<func_name>_plain.$<method_name>")
    // complex variant to include fastFormat
    //code = code.replace(/^"use strict";/, "\"use strict\";\nimport {fullFormats, fastFormats} from \"ajv-formats/dist/formats.js\";")
    //const re_formats = new RegExp("require\\(\"ajv-formats/dist/formats\"\\)\\.(?<full_or_fast>(full|fast)Formats)\\[\"(?<format>[a-z0-9-]+)\"\\];")
    //code = code.replace(re_formats, "$<full_or_fast>[\"$<format>\"];")
    code = code.replace(/^"use strict";/, "\"use strict\";\nimport {fullFormats} from \"ajv-formats/dist/formats.js\";")
    const re_formats = new RegExp("require\\(\"ajv-formats/dist/formats\"\\)\\.fullFormats\\[\"(?<format>[a-z0-9-]+)\"\\];", "gim")
    code = code.replace(re_formats, "fullFormats[\"$<format>\"];")
    const re_formats_alt = new RegExp("require\\(\"ajv-formats/dist/formats\"\\)\\.fullFormats\\.(?<format>[a-z0-9-]+);", "gim")
    code = code.replace(re_formats_alt, "fullFormats.$<format>;")
    return code;
}

for (let oasConfig of oasConfigs) {
    const ajv = new Ajv2020({strict: false, code: {source: true, es5: true, esm: true, optimize: false, lines:true}, unicodeRegExp: false})
    addFormats(ajv)
    ajv.addMetaSchema(oasConfig.schema, oasConfig.name, false)
    for (let oasOperationConfig of oasConfig.operationConfigs) {
        ajv.addSchema({$ref: oasConfig.name + oasOperationConfig.path, $schema: "https://json-schema.org/draft/2020-12/schema"}, oasOperationConfig.id + "_" + oasOperationConfig.code)
    }
    let moduleCode = standaloneCode(ajv)
    moduleCode = replace_require(moduleCode)

    fs.writeFileSync("./validate-" + oasConfig.name + "-standalone.js", moduleCode)

    const compiler = webpack({
        entry: "./validate-" + oasConfig.name + "-standalone.js",
        output: {
            filename: "./validate-" + oasConfig.name + "-bundle.js",
            library: {
                type: "module",
            },
        },
        experiments: {
            outputModule: true,
        },
        cache: false,
        module: {
            // https://webpack.js.org/loaders/babel-loader/#root
            rules: [
                {
                    loader: 'babel-loader',
                    options: {
                        plugins: [
                            [
                                "@babel/plugin-transform-unicode-property-regex",
                                { useUnicodeFlag: false }
                            ],
                            [
                                "@babel/plugin-transform-spread"
                            ],
                            [
                                "@babel/plugin-transform-object-rest-spread"
                            ]
                        ]
                    }
                }
            ],
        },
        mode: 'production',
        optimization: {
            minimize: false,
        },
        stats: 'verbose',
    });

    compiler.run((err, stats) => {
        // TODO: handle errors
        if (err) {
            console.log(stats)
        }

        let bundle = fs.readFileSync("./dist/validate-" + oasConfig.name + "-bundle.js")
        // the export line needs to be at the beginning of the file, otherwise this leads to an error. I don't know why.
        const thisisit = bundle.toString().replace(/^(.*)export (.*)$/s, "export $2$1")
        fs.writeFileSync(outputDirectory + "/validate-" + oasConfig.name + ".js", thisisit)

        compiler.close((closeErr) => {
            // ...
            fs.rmSync("./validate-" + oasConfig.name + "-standalone.js")
            fs.rmSync("./dist/validate-" + oasConfig.name + "-bundle.js")
            try {
                fs.rmdirSync("./dist")
            } catch (e) {
                // ignore. Last parallel run will clear directory
            }
        });
    });
}
// */
